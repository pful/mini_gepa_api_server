﻿using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPABusinessLogicLayer.Services;
using MiniGEPADataLayer;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPABusinessLogicLayer
{
    public class BLFacade
    {
        public static IUserService CreateUserService()
        {
            return new UserService();
        }

        public static ISalesOrderService CreateSalesOrderService()
        {
            return new SalesOrderService();
        }

        public static ISalesOrderCodeService CreateSalesOrderCodeService()
        {
            return new SalesOrderCodeService();
        }

        public static ISalesOrderEstimateSheetService CreateSalesOrderEstimateSheetService()
        {
            return new SalesOrderEstimateSheetService();
        }

        public static IFunctionPermissionService CreateFunctionPermissionService()
        {
            return new FunctionPermissionService();
        }
    }
}
