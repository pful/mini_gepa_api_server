﻿using System;
using System.Runtime.Serialization;

namespace MiniGEPABusinessLogicLayer.Exceptions
{
    public enum MiniGEPABusinessLayerErrorCode
    {
        ServiceCreationFailure,
        ValidatorNotExisted, //DataException
        EntityValidationFailure, // ValidationException ??????
        EntityCreationFailure, // ArgumentException(ContractForIRepository), 
        EntityDeletionFailure,
        GettingEntityByIdFailure,
        GettingEntitiesFailure,
        UpdatingFailure,
        WrongBusinessLogic
    }

    public class BusinessLogicLayerException : Exception
    {
        public MiniGEPABusinessLayerErrorCode ErrorCode { get; private set; }

        public BusinessLogicLayerException(MiniGEPABusinessLayerErrorCode errorCode)
        {
            ErrorCode = errorCode;
        }

        public BusinessLogicLayerException(MiniGEPABusinessLayerErrorCode errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }

        public BusinessLogicLayerException(MiniGEPABusinessLayerErrorCode errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        protected BusinessLogicLayerException(MiniGEPABusinessLayerErrorCode errorCode, SerializationInfo info, StreamingContext context) : base(info, context)
        {
            ErrorCode = errorCode;
        }
    }
}
