﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGEPABusinessLogicLayer.Exceptions
{
    internal class MiniGEPAException : Exception
    {
        public MiniGEPAException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
