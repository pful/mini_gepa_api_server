﻿using System.Collections.Generic;
using System.Web.Http.OData;

namespace MiniGEPABusinessLogicLayer.ServiceInterfaces
{
    public interface IEntityService<TEntity> where TEntity : class
    {
        void Create(TEntity entity);

        void Delete(int key);

        TEntity GetById(int key);

        IEnumerable<TEntity> GetEntities();

        void Update(TEntity entity);
    }
}
