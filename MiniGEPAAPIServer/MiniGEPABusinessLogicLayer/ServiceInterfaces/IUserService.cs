﻿using System.Web.Http.OData;
using MiniGEPAEntities;

namespace MiniGEPABusinessLogicLayer.ServiceInterfaces
{
    public interface IUserService : IEntityService<User>
    {
        User Patch(int key, Delta<User> delta);
    }
}
