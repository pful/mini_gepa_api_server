﻿using MiniGEPAEntities;

namespace MiniGEPABusinessLogicLayer.ServiceInterfaces
{
    public interface ISalesOrderEstimateSheetService : IEntityService<SalesOrderEstimateSheet>
    {
    }
}
