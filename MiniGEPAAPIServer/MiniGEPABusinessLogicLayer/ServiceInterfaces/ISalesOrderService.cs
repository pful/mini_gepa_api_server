﻿using System.Collections;
using System.Web.Http.OData;
using MiniGEPAEntities;

namespace MiniGEPABusinessLogicLayer.ServiceInterfaces
{
    public interface ISalesOrderService : IEntityService<SalesOrder>
    {
        SalesOrder Patch(int key, Delta<SalesOrder> delta);
        SalesOrder Put(int key, Delta<SalesOrder> delta);

        SalesOrderEstimateSheet NewSalesOrderEstimateSheet(SalesOrder salesOrder);

        SalesOrderEstimateSheet SelectSalesOrderEstimateSheet(
            SalesOrder salesOrder,
            int selectedSalesOrderEstimateSheetId);


    }
}
