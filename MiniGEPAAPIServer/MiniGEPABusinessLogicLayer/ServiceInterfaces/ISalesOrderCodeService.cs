﻿using MiniGEPAEntities;

namespace MiniGEPABusinessLogicLayer.ServiceInterfaces
{
    public interface ISalesOrderCodeService : IEntityService<SalesOrderCode>
    {
    }
}