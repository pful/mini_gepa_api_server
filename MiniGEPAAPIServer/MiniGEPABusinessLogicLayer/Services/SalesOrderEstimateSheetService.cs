﻿using System.Collections.Generic;
using CommonDAL.Interfaces;
using MiniGEPADataLayer;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPABusinessLogicLayer.Services
{
    internal class SalesOrderEstimateSheetService : MiniGEPABusinessLogicLayer.ServiceInterfaces.ISalesOrderEstimateSheetService
    {
        //private readonly IGEPAUnitOfWork _unitOfWork;
        private IEntityValidator<SalesOrderEstimateSheet> _validator;

        public SalesOrderEstimateSheetService()
        {
            //_unitOfWork = DALFacade.CreateUnitOfWork();
            _validator = DALFacade.GetValidator<SalesOrderEstimateSheet>();
        }

        public void Create(SalesOrderEstimateSheet entity)
        {
            _validator.Validate(entity);
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.SalesOrderEstimateSheetRepository.Insert(entity);
            unitOfWork.Save();
        }

        public void Delete(int key)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.SalesOrderEstimateSheetRepository.DeleteById(key);
            unitOfWork.Save();
        }

        public SalesOrderEstimateSheet GetById(int key)
        {
            return DALFacade.CreateUnitOfWork().SalesOrderEstimateSheetRepository.GetById(key);
        }

        public IEnumerable<SalesOrderEstimateSheet> GetEntities()
        {
            return DALFacade.CreateUnitOfWork().SalesOrderEstimateSheetRepository.Get();
        }

        public void Update(SalesOrderEstimateSheet entity)
        {
            _validator.Validate(entity);
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.SalesOrderEstimateSheetRepository.Update(entity);
            unitOfWork.Save();
        }
    }
}