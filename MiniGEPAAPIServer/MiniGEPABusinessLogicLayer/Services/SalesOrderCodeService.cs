﻿using System.Collections.Generic;
using CommonDAL.Interfaces;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPADataLayer;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPABusinessLogicLayer.Services
{
    internal class SalesOrderCodeService : ISalesOrderCodeService
    {
        //private readonly IGEPAUnitOfWork _unitOfWork;
        private IEntityValidator<SalesOrderCode> _validator;

        public SalesOrderCodeService()
        {
            //_unitOfWork = DALFacade.CreateUnitOfWork();
            _validator = DALFacade.GetValidator<SalesOrderCode>();
        }

        public void Create(SalesOrderCode entity)
        {
            _validator.Validate(entity);
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.SalesOrderCodeRepository.Insert(entity);
            unitOfWork.Save();
        }

        public void Delete(int key)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.SalesOrderCodeRepository.DeleteById(key);
            unitOfWork.Save();
        }

        public SalesOrderCode GetById(int key)
        {
            return DALFacade.CreateUnitOfWork().SalesOrderCodeRepository.GetById(key);
        }

        public IEnumerable<SalesOrderCode> GetEntities()
        {
            return DALFacade.CreateUnitOfWork().SalesOrderCodeRepository.Get();
        }

        public void Update(SalesOrderCode entity)
        {
            _validator.Validate(entity);
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.SalesOrderCodeRepository.Update(entity);
            unitOfWork.Save();
        }
    }
}