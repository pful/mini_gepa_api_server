﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using System.Web.Http.OData;
using System.Web.Http.Results;
using CommonDAL.Interfaces;
using MiniGEPADataLayer;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPABusinessLogicLayer.Services
{
    internal class UserService : ServiceInterfaces.IUserService
    {
        //private readonly IGEPAUnitOfWork _unitOfWork;
        private IEntityValidator<User> _validator;

        public UserService()
        {
            // _unitOfWork = DALFacade.CreateUnitOfWork();
            _validator = DALFacade.GetValidator<User>();
        }

        public void Create(User entity)
        {
            _validator.Validate(entity);
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.UserRepository.Insert(entity);
            unitOfWork.Save();
        }

        public void Delete(int key)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.UserRepository.DeleteById(key);
            unitOfWork.Save();
        }

        public User GetById(int key)
        {
            var user = DALFacade.CreateUnitOfWork().UserRepository.GetById(key);
            if (user == null)
                throw new NullReferenceException("There is no user has the key " + Convert.ToString(key));

            return user;
            // return DALFacade.CreateUnitOfWork().UserRepository.GetById(key);
        }
        
        public IEnumerable<User> GetEntities()
        {
            return DALFacade.CreateUnitOfWork().UserRepository.Get();
        }

        public void Update(User entity)
        {
            _validator.Validate(entity);
            var unitOfWork = DALFacade.CreateUnitOfWork();
            //unitOfWork.UserRepository.Update(entity);
            unitOfWork.Save();
        }

        public User Patch(int key, Delta<User> delta)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            var changedUser = unitOfWork.UserRepository.GetById(key);
            if (changedUser == null)
            {
                throw new NullReferenceException("Id " + Convert.ToString(key) +"를 가진 User가 없습니다.");
            }
            delta.Patch(changedUser);
            unitOfWork.UserRepository.Update(changedUser);
            unitOfWork.Save();

            return changedUser;
        }
    }
}