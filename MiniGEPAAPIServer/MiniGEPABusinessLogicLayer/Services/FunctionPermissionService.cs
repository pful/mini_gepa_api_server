﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPADataLayer;
using MiniGEPAEntities;

namespace MiniGEPABusinessLogicLayer.Services
{
    internal class FunctionPermissionService : IFunctionPermissionService
    {
        public void Create(FunctionPermission entity)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.FunctionPermissionRepository.Insert(entity);
            unitOfWork.Save();
        }

        public void Delete(int key)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.FunctionPermissionRepository.DeleteById(key);
            unitOfWork.Save();
        }

        public FunctionPermission GetById(int key)
        {
            return DALFacade.CreateUnitOfWork().FunctionPermissionRepository.GetById(key);
        }

        public IEnumerable<FunctionPermission> GetEntities()
        {
            return DALFacade.CreateUnitOfWork().FunctionPermissionRepository.Get();
        }

        public void Update(FunctionPermission entity)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            unitOfWork.FunctionPermissionRepository.Update(entity);
            unitOfWork.Save();
        }
    }
}
