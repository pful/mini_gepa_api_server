﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http.OData;
using CommonDAL.Interfaces;
using CommonDAL.ValidatationException;
using MiniGEPABusinessLogicLayer.Exceptions;
using MiniGEPADataLayer;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPABusinessLogicLayer.Services
{
    internal class SalesOrderService : ServiceInterfaces.ISalesOrderService
    {
        //private readonly IGEPAUnitOfWork _unitOfWork;
        private IEntityValidator<SalesOrder> _validator;
        private const string SalesOrder = "'SalesOrder'";

        public SalesOrderService()
        {
            // _unitOfWork = DALFacade.CreateUnitOfWork();
            try
            {
                _validator = DALFacade.GetValidator<SalesOrder>();
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.ValidatorNotExisted, 
                    ConfigurationManager.AppSettings["ValidatorNotExisted"] + SalesOrder, 
                    e);
            }
        }

        public void Create(SalesOrder entity)
        {
            try
            {
                _validator.Validate(entity);
                var unitOfWork = DALFacade.CreateUnitOfWork();
                unitOfWork.SalesOrderRepository.Insert(entity);
                unitOfWork.SalesOrderCodeRepository.Insert(new SalesOrderCode() {SalesOrder = entity});
                unitOfWork.SalesOrderEstimateSheetRepository.Insert(
                    new SalesOrderEstimateSheet() {SalesOrder = entity, Active = true, TotalCost = 0});
                unitOfWork.Save();
            }
            catch (ValidationException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.EntityValidationFailure,
                    ConfigurationManager.AppSettings["EntityValidationFailure"] ,
                    e);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.EntityCreationFailure,
                    ConfigurationManager.AppSettings["EntityCreationFailure"] + SalesOrder,
                    e);
            }
        }
        
        // sales order의 delete에서 Status가 deleted로 설정되는 update가 일어난다. 
        public void Delete(int key)
        {
            try
            {
                var unitOfWork = DALFacade.CreateUnitOfWork();
                unitOfWork.SalesOrderRepository.GetById(key).Status = 8;
                unitOfWork.Save();
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.EntityDeletionFailure,
                    ConfigurationManager.AppSettings["EntityDeletionFailure"] + SalesOrder,
                    e);
            }
        }

        public SalesOrder GetById(int key)
        {
            try
            {
                return DALFacade.CreateUnitOfWork().SalesOrderRepository.GetById(key);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.GettingEntityByIdFailure,
                    ConfigurationManager.AppSettings["GettingEntityByIdFailure"] + SalesOrder,
                    e);
            }
        }

        public IEnumerable<SalesOrder> GetEntities()
        {
            try
            {
                return DALFacade.CreateUnitOfWork().SalesOrderRepository.Get(salesOrder => salesOrder.Status != 8);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.GettingEntitiesFailure,
                    ConfigurationManager.AppSettings["GettingEntitiesFailure"] + SalesOrder,
                    e);
            }
        }

        public void Update(SalesOrder entity)
        {
            try
            {
                _validator.Validate(entity);
                var unitOfWork = DALFacade.CreateUnitOfWork();
                unitOfWork.SalesOrderRepository.Update(entity);
                unitOfWork.Save();
            }
            catch (ValidationException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.EntityValidationFailure,
                    ConfigurationManager.AppSettings["EntityValidationFailure"],
                    e);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.UpdatingFailure,
                    ConfigurationManager.AppSettings["UpdatingFailure"] + SalesOrder,
                    e);
            }
        }

        public SalesOrder Patch(int key, Delta<SalesOrder> delta)
        {
            try
            {
                var unitOfWork = DALFacade.CreateUnitOfWork();
                var changedSalesOrder = unitOfWork.SalesOrderRepository.GetById(key);
                if (changedSalesOrder == null)
                    throw new NullReferenceException();
                delta.Patch(changedSalesOrder);
                unitOfWork.SalesOrderRepository.Update(changedSalesOrder);
                unitOfWork.Save();

                return changedSalesOrder;
            }
            catch (NullReferenceException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.UpdatingFailure,
                    "No sales_order to update.",
                    e);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.UpdatingFailure,
                    "Patch failed.",
                    e);
            }
        }

        public SalesOrder Put(int key, Delta<SalesOrder> delta)
        {
            try
            {
                var unitOfWork = DALFacade.CreateUnitOfWork();
                var changedSalesOrder = unitOfWork.SalesOrderRepository.GetById(key);
                if (changedSalesOrder == null)
                    throw new NullReferenceException();
                delta.Put(changedSalesOrder);
                unitOfWork.SalesOrderRepository.Update(changedSalesOrder);
                unitOfWork.Save();

                return changedSalesOrder;
            }
            catch (NullReferenceException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.UpdatingFailure,
                    "No sales_order to update.",
                    e);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.UpdatingFailure,
                    "Patch failed.",
                    e);
            }
        }

        public SalesOrderEstimateSheet NewSalesOrderEstimateSheet(SalesOrder salesOrder)
        {
            try
            {
                var unitOfWork = DALFacade.CreateUnitOfWork();
                var activeSalesOrderEstimateSheet =
                    unitOfWork.SalesOrderEstimateSheetRepository.Get(x => x.SalesOrderId == salesOrder.Id && x.Active)
                              .Single();

                activeSalesOrderEstimateSheet.Active = false;
                var newSalesOrderEstimateSheet = new SalesOrderEstimateSheet()
                                                 {
                                                     SalesOrderId = salesOrder.Id,
                                                     Active = true,
                                                     CreatedTime = DateTime.Now,
                                                     TotalCost = 0
                                                 };

                unitOfWork.SalesOrderEstimateSheetRepository.Update(activeSalesOrderEstimateSheet);
                unitOfWork.SalesOrderEstimateSheetRepository.Insert(newSalesOrderEstimateSheet);
                unitOfWork.Save();

                return newSalesOrderEstimateSheet;
            }
            catch (ArgumentNullException e)
            {
                // Get의 결과가 null이다. 이것은 견적서와 관련된 Business Logic의 오류! 
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.WrongBusinessLogic,
                    "There is no active sales_order_estimate_sheet in sales_order " + Convert.ToString(salesOrder.Id),
                    e);
            }
            catch (InvalidOperationException e)
            {
                // Get의 결과가 여러 개일 경우. 이거서도 견적서와 관련된 Business Logic의 오류!
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.WrongBusinessLogic,
                    "There are two or more active sales_order_estimate_sheet in sales_order " +
                    Convert.ToString(salesOrder.Id),
                    e);
            }
            catch (DataLayerException e)
            {
                throw new BusinessLogicLayerException(
                    MiniGEPABusinessLayerErrorCode.WrongBusinessLogic,
                    "NewSalesOrderEstimateSheet action failed.",
                    e);
            }
        }

        public SalesOrderEstimateSheet SelectSalesOrderEstimateSheet(
            SalesOrder salesOrder,
            int selectedSalesOrderEstimateSheetId)
        {
            var unitOfWork = DALFacade.CreateUnitOfWork();
            var activeSalesOrderEstimateSheet = unitOfWork.SalesOrderEstimateSheetRepository.Get(x => x.SalesOrderId == salesOrder.Id && x.Active)
                                                                                            .SingleOrDefault();

            if (activeSalesOrderEstimateSheet == null)
                throw new NullReferenceException("주문서" + Convert.ToString(salesOrder.Id) + "에 활성화 상태인 견적서가 없습니다.");

            activeSalesOrderEstimateSheet.Active = false;

            var selectedSalesOrderEstimateSheet =
                unitOfWork.SalesOrderEstimateSheetRepository.GetById(selectedSalesOrderEstimateSheetId);
            if (selectedSalesOrderEstimateSheet == null)
                throw new NullReferenceException("Id " + Convert.ToString(selectedSalesOrderEstimateSheetId) +"를 가진 견적서가 없습니다.");

            selectedSalesOrderEstimateSheet.Active = true;

            unitOfWork.SalesOrderEstimateSheetRepository.Update(activeSalesOrderEstimateSheet);
            unitOfWork.SalesOrderEstimateSheetRepository.Update(selectedSalesOrderEstimateSheet);
            unitOfWork.Save();

            return selectedSalesOrderEstimateSheet;
        }
    }
}