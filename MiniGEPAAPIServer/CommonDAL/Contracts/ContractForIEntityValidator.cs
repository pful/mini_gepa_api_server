﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;

namespace CommonDAL.Contracts
{
    [ContractClassFor(typeof(IEntityValidator<>))]
    internal abstract class ContractForIEntityValidator<TEntity> :
        IEntityValidator<TEntity> where TEntity : class
    {
        #region

        public void Validate(TEntity entity)
        {
            Contract.Requires<ArgumentNullException>(entity != null);
        }

        #endregion
    }
}
