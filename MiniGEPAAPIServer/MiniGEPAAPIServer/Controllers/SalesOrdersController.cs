﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MiniGEPAAPIServer.Context;
using MiniGEPABusinessLogicLayer;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MiniGEPAEntities;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SalesOrder>("SalesOrders");
    builder.EntitySet<SalesOrderCode>("SalesOrderCodes"); 
    builder.EntitySet<SalesOrderEstimateSheet>("SalesOrderEstimateSheets"); 
    builder.EntitySet<User>("Users"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SalesOrdersController : ODataController
    {
        private readonly ISalesOrderService _service = BLFacade.CreateSalesOrderService();
        private GEPAContext db = new GEPAContext();

        // GET: odata/SalesOrders
        [EnableQuery]
        public IQueryable<SalesOrder> GetSalesOrders()
        {
            return _service.GetEntities().AsQueryable();
        }

        // GET: odata/SalesOrders(5)
        [EnableQuery]
        public SalesOrder GetSalesOrder([FromODataUri] int key)
        {
            return _service.GetById(key);
        }

        // PUT: odata/SalesOrders(5)  // SalesOrder의 Status는 변경이 불가능해야 한다.
        public IHttpActionResult Put([FromODataUri] int key, Delta<SalesOrder> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var changedUser = _service.Put(key, patch);

            return Updated(changedUser);
        }

        // POST: odata/SalesOrders
        public IHttpActionResult Post(SalesOrder salesOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            salesOrder.CreatedDate = DateTime.Now;
            _service.Create(salesOrder);

            return Created(salesOrder);
        }

        // PATCH: odata/SalesOrders(5)   // SalesOrder의 Status는 변경이 불가능해야 한다.
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<SalesOrder> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var changedUser = _service.Patch(key, patch);

            return Updated(changedUser);
        }

        // DELETE: odata/SalesOrders(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            var salesOrder = _service.GetById(key);
            if (salesOrder == null)
            {
                return NotFound();
            }

            _service.Delete(key);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SalesOrders(5)/SalesOrderCode
        [EnableQuery]
        public SalesOrderCode GetSalesOrderCode([FromODataUri] int key)
        {
            return _service.GetById(key).SalesOrderCode;
        }

        // GET: odata/SalesOrders(5)/SalesOrderEstimateSheets
        [EnableQuery]
        public IQueryable<SalesOrderEstimateSheet> GetSalesOrderEstimateSheets([FromODataUri] int key)
        {
            return _service.GetById(key).SalesOrderEstimateSheets.AsQueryable();
        }

        // GET: odata/SalesOrders(5)/User
        [EnableQuery]
        public SingleResult<User> GetUser([FromODataUri] int key)
        {
            return SingleResult.Create(new [] {_service.GetById(key).User}.AsQueryable());
        }
        
        [HttpPost]
        // Post: odata/SalesOrders(5)/SalesOrderEstimateSheet
        public IHttpActionResult NewSalesOrderEstimateSheet([FromODataUri] int key)
        {
            /*
             * Contract : Requires
             * 1) SalesOrder가 활성화 상태
             * 2) EstimateSheet의 ModelValidation이 MiniGEPAEntities library에서 하게 되어야 한다. 하지만 원래 생성되는 코드에서처럼 이 함수 내부에서도 ModelValidate 거치기
             * 
             * Contract : Ensure
             * 1) 기존에 있던 SalesOrderEstimateSheet가 비활성화 상태
             * 2) 새로 생긴 SalesOrderEstimateSheet는 active = true
             * 3) SalesOrderStatus는 변함없음
             */

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var salesOrder = _service.GetById(key);
            if (salesOrder == null)
            {
                return NotFound();
            }

            var salesOrderEstimateSheet = _service.NewSalesOrderEstimateSheet(salesOrder);
            

            return Created(salesOrderEstimateSheet);
        }

        // SalesOrder(1)/SelectSalesOrderEstimateSheet
        public IHttpActionResult SelectSalesOrderEstimateSheet([FromODataUri] int key, ODataActionParameters parameters)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var selectedSalesOrderEstimateSheetId = (int) parameters["SelectedSalesOrderEstmateSheetId"];

            var salesOrder = _service.GetById(key);
            if (salesOrder == null)
            {
                return NotFound();
            }

            var selectedSalesOrderEstimateSheet = _service.SelectSalesOrderEstimateSheet(
                salesOrder,
                selectedSalesOrderEstimateSheetId);

            return Updated(selectedSalesOrderEstimateSheet);
        }

        private bool SalesOrderExists(int key)
        {
            return _service.GetById(key) != null;
        }
    }
}
