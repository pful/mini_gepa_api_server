﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using MiniGEPAAPIServer.Context;
using MiniGEPABusinessLogicLayer;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPAAPIServer.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MiniGEPAEntities;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SalesOrderCode>("SalesOrderCodes");
    builder.EntitySet<SalesOrder>("SalesOrders"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SalesOrderCodesController : ODataController
    {
        private readonly ISalesOrderCodeService _service = BLFacade.CreateSalesOrderCodeService();
        private IGEPAContext dd = new GEPAContext();

        // GET: odata/SalesOrderCodes
        [EnableQuery]
        public IQueryable<SalesOrderCode> GetSalesOrderCodes()
        {
            return _service.GetEntities().AsQueryable();
        }

        // GET: odata/SalesOrderCodes(5)
        [EnableQuery]
        public SalesOrderCode GetSalesOrderCode([FromODataUri] int key)
        {
            return _service.GetEntities().SingleOrDefault(salesOrderCode => salesOrderCode.SalesOrderId == key);
        }

        // PUT: odata/SalesOrderCodes(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<SalesOrderCode> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var salesOrderCode = _service.GetById(key);//await db.SalesOrderCodes.FindAsync(key);
            if (salesOrderCode == null)
            {
                return NotFound();
            }

            patch.Put(salesOrderCode);

            try
            {
                _service.Update(salesOrderCode);
                //await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesOrderCodeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(salesOrderCode);
        }

        // POST: odata/SalesOrderCodes
        public async Task<IHttpActionResult> Post(SalesOrderCode salesOrderCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Create(salesOrderCode);
            //db.SalesOrderCodes.Add(salesOrderCode);

            /*try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SalesOrderCodeExists(salesOrderCode.SalesOrderId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }*/

            return Created(salesOrderCode);
        }

        // PATCH: odata/SalesOrderCodes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<SalesOrderCode> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var salesOrderCode = _service.GetById(key); //await db.SalesOrderCodes.FindAsync(key);
            if (salesOrderCode == null)
            {
                return NotFound();
            }

            patch.Patch(salesOrderCode);

            try
            {
                _service.Update(salesOrderCode);
                //await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesOrderCodeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(salesOrderCode);
        }

        // DELETE: odata/SalesOrderCodes(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            var salesOrderCode = _service.GetById(key);//await db.SalesOrderCodes.FindAsync(key);
            if (salesOrderCode == null)
            {
                return NotFound();
            }

            _service.Delete(key);
            /*db.SalesOrderCodes.Remove(salesOrderCode);
            await db.SaveChangesAsync();*/

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SalesOrderCodes(5)/SalesOrder
        [EnableQuery]
        public SalesOrder GetSalesOrder([FromODataUri] int key)
        {
            return _service.GetById(key).SalesOrder;
            //return SingleResult.Create(db.SalesOrderCodes.Where(m => m.SalesOrderId == key).Select(m => m.SalesOrder));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private bool SalesOrderCodeExists(int key)
        {
            return _service.GetById(key) != null;
        }
    }
}
