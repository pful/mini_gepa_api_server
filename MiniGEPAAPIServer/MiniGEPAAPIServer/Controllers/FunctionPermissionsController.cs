﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using MiniGEPAAPIServer.Context;
using MiniGEPABusinessLogicLayer;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MiniGEPAEntities;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<FunctionPermission>("FunctionPermissions");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class FunctionPermissionsController : ODataController
    {
        private readonly IFunctionPermissionService _service = BLFacade.CreateFunctionPermissionService();
        private GEPAContext db = new GEPAContext();

        // GET: odata/FunctionPermissions
        [EnableQuery]
        public IQueryable<FunctionPermission> GetFunctionPermissions()
        {
            return _service.GetEntities().AsQueryable();
        }

        // GET: odata/FunctionPermissions(5)
        [EnableQuery]
        public SingleResult<FunctionPermission> GetFunctionPermission([FromODataUri] int key)
        {
            return SingleResult.Create(new[] {_service.GetById(key)}.AsQueryable());
        }

        // PUT: odata/FunctionPermissions(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<FunctionPermission> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FunctionPermission functionPermission = db.FunctionPermissions.Find(key);
            if (functionPermission == null)
            {
                return NotFound();
            }

            patch.Put(functionPermission);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FunctionPermissionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(functionPermission);
        }

        // POST: odata/FunctionPermissions
        public IHttpActionResult Post(FunctionPermission functionPermission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.FunctionPermissions.Add(functionPermission);
            db.SaveChanges();

            return Created(functionPermission);
        }

        // PATCH: odata/FunctionPermissions(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<FunctionPermission> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FunctionPermission functionPermission = db.FunctionPermissions.Find(key);
            if (functionPermission == null)
            {
                return NotFound();
            }

            patch.Patch(functionPermission);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FunctionPermissionExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(functionPermission);
        }

        // DELETE: odata/FunctionPermissions(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            FunctionPermission functionPermission = db.FunctionPermissions.Find(key);
            if (functionPermission == null)
            {
                return NotFound();
            }

            db.FunctionPermissions.Remove(functionPermission);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FunctionPermissionExists(int key)
        {
            return db.FunctionPermissions.Count(e => e.Id == key) > 0;
        }
    }
}
