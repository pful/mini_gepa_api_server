﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using MiniGEPABusinessLogicLayer;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MiniGEPAEntities;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SalesOrderEstimateSheet>("SalesOrderEstimateSheets");
    builder.EntitySet<SalesOrder>("SalesOrders"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SalesOrderEstimateSheetsController : ODataController
    {
        private readonly ISalesOrderEstimateSheetService _salesOrderEstimateSheetServiceService =
            BLFacade.CreateSalesOrderEstimateSheetService();

        // GET: odata/SalesOrderEstimateSheets
        [EnableQuery]
        public IQueryable<SalesOrderEstimateSheet> GetSalesOrderEstimateSheets()
        {
            return _salesOrderEstimateSheetServiceService.GetEntities().AsQueryable();
        }

        // GET: odata/SalesOrderEstimateSheets(5)
        [EnableQuery]
        public SalesOrderEstimateSheet GetSalesOrderEstimateSheet([FromODataUri] int key)
        {
            return _salesOrderEstimateSheetServiceService.GetById(key);
        }

        // PUT: odata/SalesOrderEstimateSheets(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<SalesOrderEstimateSheet> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var salesOrderEstimateSheet = _salesOrderEstimateSheetServiceService.GetById(key);
            if (salesOrderEstimateSheet == null)
            {
                return NotFound();
            }

            patch.Put(salesOrderEstimateSheet);

            try
            {
                _salesOrderEstimateSheetServiceService.Update(salesOrderEstimateSheet);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesOrderEstimateSheetExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(salesOrderEstimateSheet);
        }

        // POST: odata/SalesOrderEstimateSheets
        public IHttpActionResult Post(SalesOrderEstimateSheet salesOrderEstimateSheet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _salesOrderEstimateSheetServiceService.Create(salesOrderEstimateSheet);

            return Created(salesOrderEstimateSheet);
        }

        // PATCH: odata/SalesOrderEstimateSheets(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<SalesOrderEstimateSheet> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var salesOrderEstimateSheet = _salesOrderEstimateSheetServiceService.GetById(key);
            if (salesOrderEstimateSheet == null)
            {
                return NotFound();
            }

            patch.Patch(salesOrderEstimateSheet);

            try
            {
                _salesOrderEstimateSheetServiceService.Update(salesOrderEstimateSheet);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SalesOrderEstimateSheetExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(salesOrderEstimateSheet);
        }

        // DELETE: odata/SalesOrderEstimateSheets(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            var salesOrderEstimateSheet = _salesOrderEstimateSheetServiceService.GetById(key);
            if (salesOrderEstimateSheet == null)
            {
                return NotFound();
            }

            _salesOrderEstimateSheetServiceService.Delete(key);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SalesOrderEstimateSheets(5)/SalesOrder
        [EnableQuery]
        public SalesOrder GetSalesOrder([FromODataUri] int key)
        {
            return _salesOrderEstimateSheetServiceService.GetById(key).SalesOrder;
        }

        private bool SalesOrderEstimateSheetExists(int key)
        {
            return _salesOrderEstimateSheetServiceService.GetById(key) != null;
        }
    }
}
