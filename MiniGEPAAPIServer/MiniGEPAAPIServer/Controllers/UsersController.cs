﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using MiniGEPAAPIServer.Basic_Authentication.Filters;
using MiniGEPAAPIServer.ExceptionHandling.Filters;
using MiniGEPABusinessLogicLayer;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using MiniGEPAEntities;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<User>("Users");
    builder.EntitySet<SalesOrder>("SalesOrders"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [IdentityBasicAuthenticationFilter]
    [Authorize]
    [ExceptionFilter]
    public class UsersController : ODataController
    {
        private readonly IUserService _userService = BLFacade.CreateUserService();
        //private GEPAContext db = new GEPAContext();

        // GET: odata/Users
        [EnableQuery]
        public IQueryable<User> GetUsers()
        {
            return _userService.GetEntities().AsQueryable();
        }

        // GET: odata/Users(5)
        [EnableQuery]
        public User GetUser([FromODataUri] int key)
        {
            return _userService.GetById(key);
        }

        // PUT: odata/Users(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<User> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = _userService.GetById(key);
            if (user == null)
            {
                return NotFound();
            }

            patch.Put(user);

            _userService.Update(user);

            return Updated(user);
        }

        // POST: odata/Users
        public IHttpActionResult Post(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _userService.Create(user);

            return Created(user);
        }

        // PATCH: odata/Users(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<User> patch)
        {
            Validate(patch.GetEntity());
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = _userService.Patch(key, patch);

            return Updated(user);
        }

        // DELETE: odata/Users(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            var user = _userService.GetById(key);
            if (user == null)
            {
                return NotFound();
            }

            _userService.Delete(key);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Users(5)/SalesOrders
        [EnableQuery]
        public IQueryable<SalesOrder> GetSalesOrders([FromODataUri] int key)
        {
            return _userService.GetById(key).SalesOrders.AsQueryable();

            //return db.Users.Where(m => m.Id == key).SelectMany(m => m.SalesrOders);
        }
    }
}
