﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Services.Description;
using System.Web.UI.WebControls;
using MiniGEPABusinessLogicLayer.Exceptions;

namespace MiniGEPAAPIServer.ExceptionHandling.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is BusinessLogicLayerException)
            {
                var exception = (BusinessLogicLayerException) context.Exception;
                if (exception.ErrorCode == MiniGEPABusinessLayerErrorCode.EntityCreationFailure)
                    context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                                       {
                                           ReasonPhrase = "Internal server error"
                                       };
                else if (exception.ErrorCode == MiniGEPABusinessLayerErrorCode.ValidatorNotExisted)
                    context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                                       {
                                           ReasonPhrase = "Internal server error"
                                       };
                else if (exception.ErrorCode == MiniGEPABusinessLayerErrorCode.EntityValidationFailure)
                        context.Response = new HttpResponseMessage((HttpStatusCode) 422)
                                       {
                                           ReasonPhrase = "Unprocessable entity"
                                       };
                /*InnerException을 공개하기 위해서 다른 Exception object를 throw해야 한다.*/
            }
        }
    }
}