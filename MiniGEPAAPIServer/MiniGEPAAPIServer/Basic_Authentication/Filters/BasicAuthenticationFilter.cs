﻿using System;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using MiniGEPAAPIServer.Basic_Authentication.Extensions;
using MiniGEPAAPIServer.Basic_Authentication.Results;

namespace MiniGEPAAPIServer.Basic_Authentication.Filters
{
    public abstract class BasicAuthenticationFilter : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => false;

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var authorizationHeader = context.Request.Headers.Authorization;
            if (string.CompareOrdinal(authorizationHeader?.Scheme, "Basic") != 0)
                return;

            if (string.IsNullOrWhiteSpace(authorizationHeader.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", context.Request);
                return;
            }

            var userIdAndPassword = ExtractUserIdAndPassword(authorizationHeader.Parameter);
            if (userIdAndPassword == null)
            {
                context.ErrorResult = new AuthenticationFailureResult("Invalid credentials", context.Request);
            }

            var userId = userIdAndPassword.Item1;
            var password = userIdAndPassword.Item2;

            var principal = await AuthenticateAsync(userId, password, cancellationToken);
            if (principal == null)
                context.ErrorResult = new AuthenticationFailureResult("Invalid userId or password", context.Request);
            else
                context.Principal = principal;
        }

        protected abstract Task<IPrincipal> AuthenticateAsync(
            string userId,
            string password,
            CancellationToken cancellationToken);

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }

        private static void Challenge(HttpAuthenticationChallengeContext context)
        {
            context.ChallengeWith("Basic", context.Request.RequestUri.DnsSafeHost);
        }

        private static Tuple<string, string> ExtractUserIdAndPassword(string authorizationParameter)
        {
            byte[] credentialBytes;

            try
            {
                credentialBytes = Convert.FromBase64String(authorizationParameter);
            }
            catch (FormatException)
            {
                return null;
            }

            var encoding = (Encoding) Encoding.ASCII.Clone();
            encoding.DecoderFallback = DecoderFallback.ExceptionFallback;

            string decodedCredentials;

            try
            {
                decodedCredentials = encoding.GetString(credentialBytes);
            }
            catch (DecoderFallbackException)
            {
                return null;
            }

            if (string.IsNullOrEmpty(decodedCredentials))
                return null;

            var colonIndex = decodedCredentials.IndexOf(':');
            if (colonIndex == -1)
                return null;

            var userId = decodedCredentials.Substring(0, colonIndex);
            var password = decodedCredentials.Substring(colonIndex + 1);
            return new Tuple<string, string>(userId, password);
        }
    }
}