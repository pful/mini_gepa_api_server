﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MiniGEPAAPIServer.Authorization;
using MiniGEPAAPIServer.Authorization.EndpointToFunction;
using MiniGEPABusinessLogicLayer;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer.Basic_Authentication.Filters
{
    public class IdentityBasicAuthenticationFilter : BasicAuthenticationFilter
    {
        protected override async Task<IPrincipal> AuthenticateAsync(string userId, string password, CancellationToken cancellationToken)
        {
            var user =
                BLFacade.CreateUserService()
                        .GetEntities()
                        .SingleOrDefault(x => x.UserId == userId && x.Password == password);

            if (user == null)
                return null;

            var absolutePath = GetRequestUrl();
            if (!absolutePath.IndexOf("(", StringComparison.Ordinal).Equals(-1))
                absolutePath = absolutePath.Substring(0, absolutePath.IndexOf("(", StringComparison.Ordinal));

            var function = EndpointToFunctionMapping.MappingTable[absolutePath];
            var functionPermissionOfUser = BLFacade.CreateFunctionPermissionService()
                                                   .GetEntities()
                                                   .Single(x => x.Name == function.Name && x.RoleId == user.Role);

            // 추후에 User table의 Role을 나타내는 테이블을 추가할 것 : enum UserRoleType 
            var claims = new List<Claim>
                         {
                             new Claim(ClaimTypes.Role, Convert.ToString(user.Role)),
                             new Claim(ClaimTypes.NameIdentifier, user.UserId)
                         };

            var identity = ServiceAccessLevel.CanAccess(function, functionPermissionOfUser) ? new ClaimsIdentity(claims, "Basic") : new ClaimsIdentity(claims);
            
            return new ClaimsPrincipal(identity);
        }

        private static string GetRequestUrl()
        {
            return HttpContext.Current.Request.RawUrl;
        }

        //private string TrimStringByChar
    }
}