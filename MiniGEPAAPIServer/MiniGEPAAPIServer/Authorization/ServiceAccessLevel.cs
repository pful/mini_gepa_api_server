﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer.Authorization
{
    public class ServiceAccessLevel
    {
        public static bool CanAccess(Function function, FunctionPermission functionPermission)
        {
            Contract.Requires<ArgumentNullException>(function != null, "There is no function you requested.");
            Contract.Requires<ArgumentNullException>(functionPermission != null, "There is wrong crud_type value of the user.");
            Contract.Requires<ArgumentException>(
                functionPermission.CrudType >= 0 && functionPermission.CrudType < 16,
                "Wrong CrudType value!");

            var functionType = function.ConverseFunctionTypeToInt();
            return (functionType & functionPermission.CrudType) == functionType;
        }
    }
}