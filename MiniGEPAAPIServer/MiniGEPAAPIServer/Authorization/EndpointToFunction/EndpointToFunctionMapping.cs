﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace MiniGEPAAPIServer.Authorization.EndpointToFunction
{
    public class EndpointToFunctionMapping
    {
        private static readonly string FilePath = HttpContext.Current.Server.MapPath("~/") +
                                                  "Authorization/EndpointToFunction/EndpointToFunctionMappingData.xml";

        public static Dictionary<string, Function> MappingTable { get; }

        static EndpointToFunctionMapping()
        {
            MappingTable = new Dictionary<string, Function>();
            var xmlReader = XmlReader.Create(FilePath);

            while (xmlReader.Read())
            {
                if (xmlReader.IsStartElement() && xmlReader.Name == "mapping")
                {
                    MappingTable.Add(
                        xmlReader.GetAttribute("endpoint"),
                        new Function
                        {
                            Name = xmlReader.GetAttribute("function")
                        });
                }
            }
        }
    }
}