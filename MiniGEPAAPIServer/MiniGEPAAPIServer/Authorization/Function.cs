﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiniGEPAAPIServer.Authorization
{
    public class Function
    {
        public enum Types
        {
            POST = 1,
            GET = 2,
            PUT = 4,
            PATCH = 4,
            MERGE = 4,
            DELETE = 8
        }

        public string Name { get; set; }
        public Types Type { get; set; }

        public int ConverseFunctionTypeToInt()
        {
            return (int) StringToFunctionTypes(HttpContext.Current.Request.HttpMethod);
        }

        public static Types StringToFunctionTypes(string strOfType)
        {
            return (Types) Enum.Parse(typeof (Types), strOfType);
        }
    }
}