﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Routing.Conventions;
using MiniGEPAAPIServer.ExceptionHandling.Filters;
using MiniGEPAEntities;

namespace MiniGEPAAPIServer
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new ExceptionFilter());

            var builder = new ODataConventionModelBuilder {ContainerName = "MiniGEPAContainer"};
            builder.EntitySet<User>("Users");
            builder.EntitySet<SalesOrder>("SalesOrders");
            builder.EntitySet<SalesOrderCode>("SalesOrderCodes");
            builder.EntitySet<SalesOrderEstimateSheet>("SalesOrderEstimateSheets");
            builder.EntitySet<FunctionPermission>("FunctionPermissions");

            // Add actions to the EDM, and define the parameter and return type
            ActionConfiguration newSalesOrderEstimateSheet =
                builder.Entity<SalesOrder>().Action("NewSalesOrderEstimateSheet");
            newSalesOrderEstimateSheet.ReturnsFromEntitySet<SalesOrderEstimateSheet>("SalesOrderEstimateSheets");
            ActionConfiguration selectSalesOrderEstimateSheet =
                builder.Entity<SalesOrder>().Action("SelectSalesOrderEstimateSheet");
            selectSalesOrderEstimateSheet.Parameter<int>("SelectedSalesOrderEstmateSheetId");
            selectSalesOrderEstimateSheet.ReturnsFromEntitySet<SalesOrderEstimateSheet>("SalesOrderEstimateSheets");

            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
