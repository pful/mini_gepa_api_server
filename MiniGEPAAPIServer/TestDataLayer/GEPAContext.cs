﻿using System.Data.Entity;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPADataLayer
{
    public class GEPAContext : DbContext, IGEPAContext
    {
        public GEPAContext() : base("name=GEPAContext")
        {
        }

        public GEPAContext(string connectionString) : base(connectionString)
        {
        }

        #region IGEPAContext
       
        public DbSet<User> Users { get; set; }

        public DbSet<SalesOrder> SalesOrders { get; set; }

        public DbSet<SalesOrderCode> SalesOrderCodes { get; set; }

        public DbSet<SalesOrderEstimateSheet> SalesOrderEstimateSheets { get; set; }

        public DbSet<FunctionPermission> FunctionPermissions { get; set; }

        #endregion

        // Fluent API
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configure default schema
            base.OnModelCreating(modelBuilder);

            var userEntityConfiguration = modelBuilder.Entity<User>();
            userEntityConfiguration.HasKey(x => x.Id);
            userEntityConfiguration.Property(x => x.UserId).IsRequired().HasMaxLength(12);
            userEntityConfiguration.Property(x => x.Password).IsRequired().HasMaxLength(16);
            userEntityConfiguration.Property(x => x.Role).IsRequired();
            userEntityConfiguration.Property(x => x.LastLoginTime).IsOptional();
            userEntityConfiguration.Property(x => x.LastLogoutTime).IsOptional();

            var salesOrderEntityConfiguration = modelBuilder.Entity<SalesOrder>();
            salesOrderEntityConfiguration.HasKey(x => x.Id);
            salesOrderEntityConfiguration.Property(x => x.UserId).IsRequired();
            salesOrderEntityConfiguration.Property(x => x.Status).IsRequired();
            salesOrderEntityConfiguration.Property(x => x.CreatedDate).IsRequired();
            salesOrderEntityConfiguration.HasOptional(x => x.SalesOrderCode)
                                         .WithRequired(code => code.SalesOrder);

            var salesOrderCodeEntityConfiguration = modelBuilder.Entity<SalesOrderCode>();
            salesOrderCodeEntityConfiguration.HasKey(x => x.SalesOrderId);

            var salesOrderEstimateSheetConfiguration = modelBuilder.Entity<SalesOrderEstimateSheet>();
            salesOrderEstimateSheetConfiguration.HasKey(x => x.Id);
            salesOrderEstimateSheetConfiguration.Property(x => x.SalesOrderId).IsRequired();
            salesOrderEstimateSheetConfiguration.Property(x => x.Active).IsRequired();
            salesOrderEstimateSheetConfiguration.Property(x => x.CreatedTime).IsOptional();
            salesOrderEstimateSheetConfiguration.Property(x => x.TotalCost).IsRequired();

            var functionPermissionConfiguration = modelBuilder.Entity<FunctionPermission>();
            functionPermissionConfiguration.HasKey(x => x.Id);
            functionPermissionConfiguration.Property(x => x.Name).IsRequired();
            functionPermissionConfiguration.Property(x => x.CrudType).IsRequired();
            functionPermissionConfiguration.Property(x => x.RoleId).IsRequired();
        }
    }
}
