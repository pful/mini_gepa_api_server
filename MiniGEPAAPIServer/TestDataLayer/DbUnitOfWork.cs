﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Runtime.CompilerServices;
using CommonDAL;
using CommonDAL.Interfaces;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPADataLayer
{
    internal class DbUnitOfWork : IGEPAUnitOfWork
    {
        private readonly GEPAContext _context;
        private IRepository<User> _userRepository;
        private IRepository<SalesOrder> _salesOrderRepository;
        private IRepository<SalesOrderEstimateSheet> _salesOrderEstimateSheetRepository;
        private IRepository<SalesOrderCode> _salesOrderCodeRepository;
        private IRepository<FunctionPermission> _functionPermissionRepository;

        public DbUnitOfWork(GEPAContext context)
        {
            _context = context;
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                throw new DataLayerException("DbUpdateConcurrencyException occured while updating database.", e);
            }
            catch (DbEntityValidationException e)
            {
                throw new DataLayerException("Validation of entity property values failed.", e);
            }
            catch (NotSupportedException e)
            {
                throw new DataLayerException("Unsupported behavior was required while updating database.", e);
            }
            catch (ObjectDisposedException e)
            {
                throw new DataLayerException("The DbContext or connection have been disposed.", e);
            }
            catch (InvalidOperationException e)
            {
                throw new DataLayerException("Some error occured attempting to process entities in the context either before or after sending commands to the database.", e);
            }
        }

        public IRepository<TEntity> GetRepository <TEntity>() where TEntity : class
        {
            var type = typeof(TEntity);

            try
            {
                if (type == typeof(User))
                {
                    return (IRepository<TEntity>) (_userRepository ??
                                                   (_userRepository =
                                                       new EFGenericRepository<GEPAContext, User>(_context)));
                }

                if (type == typeof(SalesOrder))
                {
                    return (IRepository<TEntity>) (_salesOrderRepository ??
                                                   (_salesOrderRepository =
                                                       new EFGenericRepository<GEPAContext, SalesOrder>(_context)));
                }

                if (type == typeof(SalesOrderCode))
                {
                    return (IRepository<TEntity>) (_salesOrderCodeRepository ??
                                                   (_salesOrderCodeRepository =
                                                       new EFGenericRepository<GEPAContext, SalesOrderCode>(_context)));
                }

                if (type == typeof(SalesOrderEstimateSheet))
                {
                    return (IRepository<TEntity>) (_salesOrderEstimateSheetRepository ??
                                                   (_salesOrderEstimateSheetRepository =
                                                       new EFGenericRepository<GEPAContext, SalesOrderEstimateSheet>(_context)));
                }

                if (type == typeof(FunctionPermission))
                {
                    return (IRepository<TEntity>) (_functionPermissionRepository ??
                                                   (_functionPermissionRepository =
                                                       new EFGenericRepository<GEPAContext, FunctionPermission>(_context)));
                }

                throw new NotSupportedException(type.ToString());
            }
            catch (NotSupportedException e)
            {
                throw new DataLayerException("There is no repository for entity " + type, e);
            } 
            // EFGenericRepository에 의해서 예외가 발생했을 때. 
            catch (Exception e/*???? which exception*/)
            {
                throw new DataLayerException("Cannot create EFGenericRepository for " + type, e);
            }
        }

        public IRepository<User> UserRepository => GetRepository<User>();
        public IRepository<SalesOrder> SalesOrderRepository => GetRepository<SalesOrder>();
        public IRepository<SalesOrderCode> SalesOrderCodeRepository => GetRepository<SalesOrderCode>();
        public IRepository<SalesOrderEstimateSheet> SalesOrderEstimateSheetRepository => GetRepository<SalesOrderEstimateSheet>();
        public IRepository<FunctionPermission> FunctionPermissionRepository => GetRepository<FunctionPermission>();
    }
}