namespace MiniGEPADataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCreatedDateToRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("SalesOrders", "CreatedDate", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            AlterColumn("SalesOrders", "CreatedDate", c => c.DateTime(precision: 0));
        }
    }
}
