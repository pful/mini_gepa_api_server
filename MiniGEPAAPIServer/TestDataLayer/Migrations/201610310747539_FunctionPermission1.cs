namespace MiniGEPADataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FunctionPermission1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("FunctionPermissions", "Name", c => c.String(nullable: false, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("FunctionPermissions", "Name", c => c.String(unicode: false));
        }
    }
}
