namespace MiniGEPADataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FunctionPermission : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "FunctionPermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        RoleId = c.Int(nullable: false),
                        CrudType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)                ;
            
        }
        
        public override void Down()
        {
            DropTable("FunctionPermissions");
        }
    }
}
