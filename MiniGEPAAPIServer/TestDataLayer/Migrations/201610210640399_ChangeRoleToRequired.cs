namespace MiniGEPADataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRoleToRequired : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "SalesOrderCodes",
                c => new
                    {
                        SalesOrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SalesOrderId)                
                .ForeignKey("SalesOrders", t => t.SalesOrderId)
                .Index(t => t.SalesOrderId);
            
            CreateTable(
                "SalesOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        CreatedDate = c.DateTime(precision: 0),
                        ApproveUserId = c.Int(),
                        ApproveReason = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)                
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "SalesOrderEstimateSheets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesOrderId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        CreatedTime = c.DateTime(precision: 0),
                        TotalCost = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)                
                .ForeignKey("SalesOrders", t => t.SalesOrderId, cascadeDelete: true)
                .Index(t => t.SalesOrderId);
            
            CreateTable(
                "Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 12, storeType: "nvarchar"),
                        Password = c.String(nullable: false, maxLength: 16, storeType: "nvarchar"),
                        Role = c.Int(nullable: false),
                        LastLoginTime = c.DateTime(precision: 0),
                        LastLogoutTime = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.Id)                ;
            
        }
        
        public override void Down()
        {
            DropForeignKey("SalesOrders", "UserId", "Users");
            DropForeignKey("SalesOrderEstimateSheets", "SalesOrderId", "SalesOrders");
            DropForeignKey("SalesOrderCodes", "SalesOrderId", "SalesOrders");
            DropIndex("SalesOrderEstimateSheets", new[] { "SalesOrderId" });
            DropIndex("SalesOrders", new[] { "UserId" });
            DropIndex("SalesOrderCodes", new[] { "SalesOrderId" });
            DropTable("Users");
            DropTable("SalesOrderEstimateSheets");
            DropTable("SalesOrders");
            DropTable("SalesOrderCodes");
        }
    }
}
