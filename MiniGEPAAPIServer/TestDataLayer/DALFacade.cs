﻿using System;
using CommonDAL.Interfaces;
using MiniGEPAEntities;
using UnifiedDAL;

namespace MiniGEPADataLayer
{
    public static class DALFacade
    {
        public static IGEPAUnitOfWork CreateUnitOfWork()
        {
            return new DbUnitOfWork(new GEPAContext());
        }

        public static IEntityValidator<TEntity> GetValidator<TEntity>() where TEntity : class
        {
            try
            {
                return EntityValidatorFactory.Create<TEntity>();
            }
            catch (NotSupportedException e)
            {       
                throw new DataLayerException("There is no EntityValidator for entity '" + typeof(TEntity) + "'", e);
            }
        }
    }
}
