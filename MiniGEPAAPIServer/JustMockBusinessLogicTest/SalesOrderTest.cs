﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiniGEPABusinessLogicLayer;
using MiniGEPABusinessLogicLayer.ServiceInterfaces;
using MiniGEPADataLayer;
using MiniGEPAEntities;
using Telerik.JustMock;
using Telerik.JustMock.AutoMock;
using Telerik.JustMock.Helpers;
using UnifiedDAL;

namespace JustMockBusinessLogicTest
{
    /// <summary>
    /// Summary description for JustMockTest
    /// </summary>
    [TestClass]
    public class SalesOrderTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        private ISalesOrderService _service;

        [TestInitialize]
        public void Initialize()
        {
            _service = BLFacade.CreateSalesOrderService();
        }

        [TestMethod]
        public void TestMethod()
        {
            // Arrange
            Mock.SetupStatic(typeof(DALFacade));

            // Insert할 array : salesorder, salesordercode, salesorderestimatesheet
            var salesOrderArrayList = new ArrayList();
            var salesOrderCodeArrayList = new ArrayList();
            var salesOrderEstimateSheetArrayList = new ArrayList();

            // new sales order
            var newSalesOrder = new SalesOrder()
                                {
                                    Id = 1,
                                    UserId = 1,
                                    Status = 1
                                };
            var newSalesOrderCode = new SalesOrderCode()
                                    {
                                        SalesOrderId = 1
                                    };
            var newSalesOrderEsteimateSheet = new SalesOrderEstimateSheet()
                                              {
                                                  Id = 1,
                                                  Active = true,
                                                  CreatedTime = DateTime.Now,
                                                  SalesOrderId = 1,
                                                  TotalCost = 0
                                              };

            // mock DbUnitOfWork 만들기
            var mockDbUnitOfWork = Mock.Create<IGEPAUnitOfWork>();
            mockDbUnitOfWork.Arrange(work => work.SalesOrderRepository.Insert(newSalesOrder))
                            .DoInstead(() => salesOrderArrayList.Add(newSalesOrder));
            /*mockDbUnitOfWork.Arrange(unitOfWork => unitOfWork.SalesOrderRepository.Insert(newSalesOrder)).Raises(
                () => salesOrderArrayList.Add(newSalesOrder));*/
            mockDbUnitOfWork.Arrange(unitOfWork => unitOfWork.SalesOrderCodeRepository.Insert(new SalesOrderCode()))
                            .Raises(
                                () =>
                                {
                                    salesOrderCodeArrayList
                                        .Add
                                        (
                                            newSalesOrderCode);
                                });
            mockDbUnitOfWork.Arrange(
                unitOfWork => unitOfWork.SalesOrderEstimateSheetRepository.Insert(newSalesOrderEsteimateSheet))
                            .Raises(
                                () =>
                                {
                                    salesOrderEstimateSheetArrayList
                                        .Add
                                        (
                                            newSalesOrderEsteimateSheet);
                                });

            mockDbUnitOfWork.Arrange(unitOfWork => unitOfWork.Save()).DoNothing();

            Mock.Arrange(() => DALFacade.CreateUnitOfWork()).Returns(mockDbUnitOfWork);

            //Act
            _service.Create(newSalesOrder);

            //Assert
            Assert.IsNotNull(salesOrderArrayList[0]);
        }
    }
}