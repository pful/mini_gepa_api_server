﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;
using MiniGEPAEntities;

namespace UnifiedDAL
{
    public interface IGEPAUnitOfWork : IUnitOfWork
    {
        IRepository<User> UserRepository { get; }
        IRepository<SalesOrder> SalesOrderRepository { get; }
        IRepository<SalesOrderCode> SalesOrderCodeRepository { get; }
        IRepository<SalesOrderEstimateSheet> SalesOrderEstimateSheetRepository { get; }
        IRepository<FunctionPermission> FunctionPermissionRepository { get; }
    }
}
