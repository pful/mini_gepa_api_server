﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniGEPAEntities;

namespace UnifiedDAL
{
    public interface IGEPAContext
    {
        DbSet<User> Users { get; set; }

        DbSet<SalesOrder> SalesOrders { get; set; }

        DbSet<SalesOrderCode> SalesOrderCodes { get; set; }

        DbSet<SalesOrderEstimateSheet> SalesOrderEstimateSheets { get; set; }

        DbSet<FunctionPermission> FunctionPermissions { get; set; }
    }
}
