﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGEPAEntities
{
    public class SalesOrder
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ApproveUserId { get; set; } // Fleunt API
        public string ApproveReason { get; set; }

        public virtual User User { get; set; }

        public virtual SalesOrderCode SalesOrderCode { get; set; }

        public virtual ICollection<SalesOrderEstimateSheet> SalesOrderEstimateSheets { get; set; }
    }
}
