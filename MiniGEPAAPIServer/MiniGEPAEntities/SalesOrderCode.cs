﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGEPAEntities
{
    public class SalesOrderCode
    {
        [Key]
        public int SalesOrderId { get; set; }
        public virtual SalesOrder SalesOrder { get; set; }
    }
}
