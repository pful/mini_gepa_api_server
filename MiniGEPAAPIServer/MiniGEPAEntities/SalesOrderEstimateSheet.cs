﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGEPAEntities
{
    public class SalesOrderEstimateSheet
    {
        [Key]
        public int Id { get; set; }
        public int SalesOrderId { get; set; }
        public SalesOrder SalesOrder { get; set; }
        public bool Active { get; set; }
        public DateTime? CreatedTime { get; set; }
        public int TotalCost { get; set; }
    }
}
