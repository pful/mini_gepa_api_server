﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;
using MiniGEPAEntities.Validators.Implementations;

namespace MiniGEPAEntities
{
    public class EntityValidatorFactory
    {
        public static IEntityValidator<TEntity> Create <TEntity>() where TEntity : class
        {
            var type = typeof(TEntity);
            
            //이 부분도.. try catch로 예외 처리를 해주어야 한다. 
            if (type == typeof(User))
                return new UserValidator() as IEntityValidator<TEntity>;

            if (type == typeof(SalesOrder))
                return new SalesOrderValidator() as IEntityValidator<TEntity>;

            if (type == typeof(SalesOrderCode))
                return new SalesOrderCodeValidator() as IEntityValidator<TEntity>;

            if (type == typeof(SalesOrderEstimateSheet))
                return new SalesOrderEstimateSheetValidator() as IEntityValidator<TEntity>;

            throw new NotSupportedException();
        }
    }
}
