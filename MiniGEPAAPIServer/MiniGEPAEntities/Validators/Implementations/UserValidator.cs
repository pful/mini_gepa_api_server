﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.ValidatationException;
using MiniGEPAEntities.Validators.Interfaces;

namespace MiniGEPAEntities.Validators.Implementations
{
    internal class UserValidator : IUserValidator
    {
        #region IEntityValidator<TEntity>

        public void Validate(User entity)
        {
            ValidateUserId(entity.UserId);
            ValidatePassword(entity.Password);
            ValidateRole(entity.Role);
        }

        #endregion

        #region IUserValidator

        public void ValidateUserId(string userId)
        {
        }

        public void ValidatePassword(string password)
        {
        }

        public void ValidateRole(int role)
        {
        }

        #endregion
    }
}