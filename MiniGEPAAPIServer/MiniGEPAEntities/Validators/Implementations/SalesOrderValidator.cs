﻿using MiniGEPAEntities.Validators.Interfaces;

namespace MiniGEPAEntities.Validators.Implementations
{
    internal class SalesOrderValidator : ISalesOrderValidator
    {
        public void Validate(SalesOrder entity)
        {
            ValidateUserId(entity.UserId);
            ValidateStatus(entity.Status);
        }

        #region ISalesOrderValidator

        public void ValidateUserId(int userId)
        {
        }

        public void ValidateStatus(int status)
        {
        }

        #endregion
    }
}
