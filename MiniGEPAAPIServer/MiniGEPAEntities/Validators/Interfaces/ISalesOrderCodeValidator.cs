﻿using System.Diagnostics.Contracts;
using CommonDAL.Interfaces;
using MiniGEPAEntities.Validators.Contracts;

namespace MiniGEPAEntities.Validators.Interfaces
{
    [ContractClass(typeof(ContractForSalesOrderCodeValidator))]
    internal interface ISalesOrderCodeValidator : IEntityValidator<SalesOrderCode>
    {

    }
}
