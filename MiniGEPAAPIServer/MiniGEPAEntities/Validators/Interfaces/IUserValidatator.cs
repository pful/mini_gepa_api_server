﻿using System.Diagnostics.Contracts;
using CommonDAL.Interfaces;
using MiniGEPAEntities.Validators.Contracts;

namespace MiniGEPAEntities.Validators.Interfaces
{
    [ContractClass(typeof(ContractForUserValidator))]
    internal interface IUserValidator : IEntityValidator<User>
    {
        void ValidateUserId(string userId);
        void ValidatePassword(string password);
        void ValidateRole(int role);
    }
}
