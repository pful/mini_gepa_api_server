﻿using System.Diagnostics.Contracts;
using CommonDAL.Interfaces;
using MiniGEPAEntities.Contracts;

namespace MiniGEPAEntities.Validators.Interfaces
{
    [ContractClass(typeof(ContractForSalesOrderEstimateSheetValidator))]
    internal interface ISalesOrderEstimateSheetValidator : IEntityValidator<SalesOrderEstimateSheet>
    {
        void ValidateSalesOrderId(int salesOrderId);
    }
}
