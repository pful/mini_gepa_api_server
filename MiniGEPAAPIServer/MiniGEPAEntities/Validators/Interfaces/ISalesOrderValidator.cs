﻿using System.Diagnostics.Contracts;
using CommonDAL.Interfaces;
using MiniGEPAEntities.Validators.Contracts;

namespace MiniGEPAEntities.Validators.Interfaces
{
    [ContractClass(typeof(ContractForSalesOrderValidator))]
    internal interface ISalesOrderValidator : IEntityValidator<SalesOrder>
    {
        void ValidateUserId(int userId);
        void ValidateStatus(int status);
    }
}
