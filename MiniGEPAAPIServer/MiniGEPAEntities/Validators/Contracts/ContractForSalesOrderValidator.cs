﻿using System;
using System.Diagnostics.Contracts;
using CommonDAL.ValidatationException;
using MiniGEPAEntities.Validators.Interfaces;

namespace MiniGEPAEntities.Validators.Contracts
{
    [ContractClassFor(typeof(ISalesOrderValidator))]
    internal abstract class ContractForSalesOrderValidator : ISalesOrderValidator
    {
        public void Validate(SalesOrder entity)
        {
            ValidateUserId(entity.UserId);
            ValidateStatus(entity.Status);
        }

        public void ValidateUserId(int userId)
        {
            Contract.Requires<ValidationException>(userId > 0);
        }

        public void ValidateStatus(int status)
        {
            Contract.Requires<ValidationException>(status > 0 && status < 8);
        }
    }
}
