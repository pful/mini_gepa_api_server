﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.ValidatationException;
using MiniGEPAEntities.Validators.Interfaces;

namespace MiniGEPAEntities.Contracts
{
    [ContractClassFor(typeof(ISalesOrderEstimateSheetValidator))]
    internal abstract class ContractForSalesOrderEstimateSheetValidator : ISalesOrderEstimateSheetValidator
    {
        public void Validate(SalesOrderEstimateSheet entity)
        {
            ValidateSalesOrderId(entity.SalesOrderId);
        }

        public void ValidateSalesOrderId(int salesOrderId)
        {
            Contract.Requires<ValidationException>(salesOrderId > 0);   
        }
    }
}
