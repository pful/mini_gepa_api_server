﻿using System.Diagnostics.Contracts;
using CommonDAL.ValidatationException;
using MiniGEPAEntities.Validators.Interfaces;

namespace MiniGEPAEntities.Validators.Contracts
{
    // request에 의해서 호출되는 타이밍! 
    [ContractClassFor(typeof(IUserValidator))]
    internal abstract class ContractForUserValidator : IUserValidator
    { 

        public void ValidateUserId(string userId)
        {
            Contract.Requires<ValidationException>(!string.IsNullOrWhiteSpace(userId));
        }

        public void ValidatePassword(string password)
        {
            Contract.Requires<ValidationException>(!string.IsNullOrWhiteSpace(password));
        }

        public void ValidateRole(int role)
        {
            Contract.Requires<ValidationException>(role > 0 && role < 6);
        }

        public void Validate(User entity)
        {
        }
    }
}
