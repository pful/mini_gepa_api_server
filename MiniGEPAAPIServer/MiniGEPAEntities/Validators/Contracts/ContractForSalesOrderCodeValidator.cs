﻿using System.Diagnostics.Contracts;
using MiniGEPAEntities.Validators.Interfaces;

namespace MiniGEPAEntities.Validators.Contracts
{
    [ContractClassFor(typeof(ISalesOrderCodeValidator))]
    internal abstract class ContractForSalesOrderCodeValidator : ISalesOrderCodeValidator
    {
        public void Validate(SalesOrderCode entity)
        {
        }
    }
}
